# Nostale Community Website


DO NOT USE IN PRODUCTION YET
--------------------

## Server Requirement
- Webserver Apache/Nginx/Litespeed
- PHP: 7.3+ with SQLSRV installed (+ PDO)
- MariaDB 10.1.X+
- Composer + SSH/CLI access

## Setup
1. Clone: `git clone https://gitlab.com/cmnos/cmnos-website.git .`
2. Setup nginx and apache to point in `/public/`
3. Rename `.env.example` to `.env`
4. Run `php artisan key:generate`
5. Edit `.env`
    0. **DO NOT EDIT** `APP_KEY`
    1. Edit Application name
    2. Edit database connection details
    3. Edit Hashing algorithm
    4. Edit gameserver database connection details
    5. Edit&Enable Google reCAPTCHA
5. Run `php atisan migrate:fresh`
6. Login with user `admin` and password `admin` ( This also work for server, you're GameMaster )

## Feature
//todo