<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
  protected $db;
  public function __construct() {
    $this->db = DB::connection('sqlsrv');
  }

  private function ranking_list(Request $request, $limit = 10) {
    return $this->db->table('Character')
      ->select(["Name", "Level", "HeroLevel", "Class"])
      ->where("Level", ">","80")
      ->orderByRaw("Level desc, HeroLevel desc")
      ->limit($limit)
      ->get();
  }

  public function ss(Request $request) {
    $da = $this->db->table('Character')->select(["Name", "Level", "HeroLevel", "Class", "Biography", "Reputation"])->where('AccountId', '=', $request->id)->get();
    dd($da);
  }

  public function report_ch_health() {
    $data = DB::table('gameserver')->get();
    $keep= [];
    for($i=0, $iMax = count($data); $i< $iMax; $i++) {
      $keep[] = [
        "title"   => $data[$i]->title,
        "status"  => $this->check_ch_health($data[$i]->ip, $data[$i]->port)
      ];
    }
    return $keep;
  }

  private function check_ch_health($ip, $port){
    return @fsockopen($ip, $port, $errno, $errstr, 0.1) ? true : false;
  }
}
