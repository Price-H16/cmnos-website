<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameAuthController extends Controller {
  protected $db;
  public function __construct() {
    $this->db = DB::connection('sqlsrv');
  }
  public function insertAccount($user, $pass, $email, $ip = null, $rid = 0, $money = 0, $authority = 0) {
    return $this->db->table('Account')->insertGetId([
      "Authority"=> $authority,
      "Email"=>$email,
      "Name"=>$user,
      "Password"=>hash("SHA512", $pass),
      "ReferrerId"=>$rid,
      "RegistrationIP"=> (is_null($ip) ? (new \Illuminate\Http\Request)->ip() : $ip),
      "DailyRewardSent"=> 0,
      "BankMoney"=>$money
    ]);
  }
  public function setPassword($email, $password){
    $this->db->table("Account")
      ->where("Email", $email)
      ->update(["Password"=> hash("SHA512", $password)]);
  }
  public function test(){
    dd(request()->ips());
  }
}
