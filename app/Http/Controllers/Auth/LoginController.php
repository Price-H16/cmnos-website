<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Logs;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use \Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

  /**
   * This trait has all the login throttling functionality.
   */
  use ThrottlesLogins;
  /**
   * Max login attempts allowed.
   */
  public $maxAttempts = 5;

  /**
   * Number of minutes to lock the login.
   */
  public $decayMinutes = 60;

  /**
   * Use username
   *
   * @return string
   */
  public function username()
    {
        return 'username';
    }

  /**
   * Send failed login response & log
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function sendFailedLoginResponse(Request $request) {
    Logs::add('Failed login username: '.$request->username,'AUTH', 'login-failed', request()->ip());
    return response()->json(base64_encode(json_encode([
      "type" => "error",
      "title"=> "Wrong Credentials",
      "msg"=>"<i class='fa fa-times prefix'></i> Username or password are wrong.",
    ])));
  }

  /**
   * Send Lockout response & log
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function sendLockoutResponse(Request $request) {
    Logs::add('Locked-out login username: '.$request->username,'AUTH', 'login-failed', request()->ip());
    return response()->json(base64_encode(json_encode([
      "type" => "error",
      "title"=> "STOP",
      "msg"=>"You're trying to hard. Take a break. Try to remeber and login!",
    ])));
  }

  /**
   * Send success response & log
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function sendLoginResponse(Request $request) {
    $user = User::find(Auth::user()->id);
    $user->ip_last = request()->ip();
    $user->save();
    Logs::add('Success login: '.$request->username,'AUTH');
    return response()->json(base64_encode(json_encode([
      "type" => "success",
      "title"=> "Login Complet",
      "msg"=>"Welcome! You will be redirected in a seconds",
    ])));
  }
}
