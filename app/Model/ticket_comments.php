<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket_comments extends Model
{
    protected $table = 'ticket_comments';

    protected $fillable = [
      'messages', 'author', 'ticket_id'
    ];

    public function ticket(){
      return $this->belongsTo(ticket::class, 'id', 'ticket_id');
    }
}
