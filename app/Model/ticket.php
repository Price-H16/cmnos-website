<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
  protected $fillable = [
    'title', 'status'
  ];

  protected $table = 'ticket';

  public function author() {
    return $this->hasOne(ticket::class, 'id', 'author');
  }

  public function messages() {
    return $this->hasMany(ticket_comments::class, 'ticket_id', 'id');
  }

}
