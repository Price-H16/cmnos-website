<?php

namespace App\Rules;

use GuzzleHttp\Client;
use Illuminate\Contracts\Validation\Rule;

class GoogleRecaptcha implements Rule
{
  /**
   * Determine if the validation rule passes.
   *
   * @param  string  $attribute
   * @param  mixed  $value
   * @return bool
   */
  public function passes($attribute, $value)
  {
    if(env('G_ENABLE', false)) {
      $client = new Client();
      $response = $client->post('https://www.google.com/recaptcha/api/siteverify',
        [
          'form_params' => [
            'secret' => env('G_SECRET', false),
            'remoteip' => request()->getClientIp(),
            'response' => $value
          ]
        ]
      );
      $body = json_decode((string)$response->getBody());
      return $body->success;
    }
    else {
      return true;
    }
  }

  /**
   * Get the validation error message.
   *
   * @return string
   */
  public function message()
  {
    return 'Are you a robot?';
  }
}