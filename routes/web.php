<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/


/* ++++++++++++++++ Public Routes ++++++++++++++++ */
Route::get('/', function () {
  $gs = new GameController();
  return view('index', [
    "server"=> $gs->report_ch_health()
  ]);
});

Route::get('/ranking', 'GameController@ranking_list')->name('ranking_list');

/* ++++++++++++++++ Authenticated Routes ++++++++++++++++ */
Auth::routes();

Route::get('/support', 'TicketController@display')->middleware('auth')->name('ticketdisplay');
Route::get('/support/ticket/{id}', 'TicketController@viewticket')->middleware('auth')->name('viewticket');

/* ++++++++++++++++ User Control Panel ++++++++++++++++ */
Route::post('user/changepassword', 'Auth\ChangePwdController@updatepassword')->middleware('auth')->name('updatepassword');
Route::post('user/changemail', 'Auth\ChangePwdController@updatemail')->middleware('auth')->name('updatemail');
Route::post('support/ticket/new', 'TicketController@create_ticket')->middleware('auth')->name('create_ticket');
Route::post('support/ticket/addreply', 'TicketController@addreply')->middleware('auth')->name('addreply_ticket');

/* ++++++++++++++++ Test Routes ++++++++++++++++ */
Route::get('/test', 'GameAuthController@test')->middleware('auth');
Route::get('/test2', function () {
  return view('ticket.ticketinside');
});