<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GameServer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gameserver', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("author")->unsigned();
            $table->foreign("author")->references("id")->on('users');
            $table->string("title", 250);
            $table->enum("type",[
              "LOGIN",
              "WORLD"
            ])->default("WORLD");
            $table->string("ip", 45);
            $table->string("port", 12);
            $table->boolean("private")->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gameserver');
    }
}
