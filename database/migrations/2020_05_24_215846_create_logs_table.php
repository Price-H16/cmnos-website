<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('logs', function (Blueprint $table) {
        $table->id();
        $table->string('env')->default(env('APP_ENV'));
        $table->string('message', 500);
        $table->enum('level', [
          'INFO',
          'LOGIN',
          'NOTICE',
          'AUTH',
          'ERROR',
        ])->default('INFO');
        $table->text('context')->nullable();
        $table->text('extra')->nullable();
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
