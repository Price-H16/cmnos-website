<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Http\Controllers\GameAuthController;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  public function run()
    {
      $gd = new GameAuthController();
      User::create([
        "username"          => "Admin",
        "email"             => "example@example.com",
        "password"          => Hash::make("Admin"),
        "email_verified_at" => now(),
        "access"            => 5,
        "created_at"        => now(),
        "reg_ip"            => "127.0.0.1",
        "ip_last"           => "127.0.0.1"
      ]);
      $gd->insertAccount("Admin", "Admin", "example@example.com", "127.0.0.1", 0, 0, 5);
    }
}
