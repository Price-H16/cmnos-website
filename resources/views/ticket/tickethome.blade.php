@extends('layouts.app')

@section('title', '- Ticket')
@section('css')
	<link rel="stylesheet" href="{{url('vendor/mybb/ui/trumbowyg.min.css')}}"/>
@endsection
@section('content')
<div class="container bg-dark rounded">
	<div class="row mb-3">
		<div class="col-12 text-center text-white">
			<h1>Ticketing Support</h1>
			<h5 class="text-light">Live support on <a class="text-success" href="https://discord.gg/g9p2p76"><i class="fab fa-discord fa-fw"></i>Discord</a></h5>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-12 text-center">
			<button class="btn bg-secondary shadow text-white" data-toggle="modal" data-target="#OpenNewTicket">Create new ticket</button>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-12">
			<table class="table table-dark table-striped text-center">
				<thead>
					<tr>
						<th>Title</th>
						<th>Status</th>
						<th>Info</th>
						<th>Open at</th>
					</tr>
				</thead>
				<tbody>
					{!! $tickets !!}
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="OpenNewTicket" tabindex="-1" role="dialog" aria-labelledby="OpenNewTicketLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg ">
		<div class="modal-content bg-dark text-white">
			<div class="modal-header">
				<h5 class="modal-title" id="OpenNewTicketLabel">Open New Ticket</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="n_ticket" method="post" action="{{url('support/ticket/new')}}">
				<div class="modal-body">
					{{csrf_field()}}
					<div class="form-group">
						<label for="title">Title of Ticket</label>
						<input type="text" class="form-control" id="title" placeholder="Title of Ticket" name="title">
					</div>
					<div class="form-group">
						<label for="modal_textarea">Example textarea</label>
						<textarea class="form-control" id="modal_textarea" rows="7" name="ticket_msg"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Dismiss</button>
					<button type="submit" class="btn btn-primary">Create Ticket</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('js')
	<script src="{{url('vendor/mybb/trumbowyg.min.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function () {
		    $('#submit').on('click','', function () {
				    $('#n_ticket').submit();
        });
        $('#modal_textarea').trumbowyg({
            btns: [
                ['undo', 'redo'], // Only supported in Blink browsers
                ['formatting'],
                ['strong', 'em', 'del'],
                ['link'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['removeformat'],
            ]
        });
    });
	</script>
@endsection