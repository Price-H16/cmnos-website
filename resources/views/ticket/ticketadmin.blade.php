<?php
use App\User;
?>
@extends('layouts.app')
@section('css')
	<link rel="stylesheet" href="{{url('vendor/datatables/datatables.min.css')}}">
@endsection
@section('title', '- Ticket Admin')
@section('content')
	<div class="container bg-dark rounded">
		<div class="row mb-3">
			<div class="col-12 text-center text-white">
				<h1>Ticketing Support</h1>
				<h5 class="text-light">Hello  GameSupport Fmohican, there are 1 open and 5 closed tickets</h5>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-12 pb-5">
				<div class="row">
					<div class="col-2">
						<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
							<a class="nav-link active" id="v-pills-open-tab" data-toggle="pill" href="#v-pills-open" role="tab" aria-controls="v-pills-open" aria-selected="true">Open</a>
							<a class="nav-link" id="v-pills-closed-tab" data-toggle="pill" href="#v-pills-closed" role="tab" aria-controls="v-pills-closed" aria-selected="false">Closed</a>
						</div>
					</div>
					<div class="col-10">
						<div class="tab-content" id="v-pills-tabContent">
							<div class="tab-pane fade show active" id="v-pills-open" role="tabpanel" aria-labelledby="v-pills-open-tab">
								<table id="open" class="table table-dark table-striped table-condensed">
									<thead>
										<tr>
											<th>User</th>
											<th>Title</th>
											<th>OpenAT</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($tickets_open as $ticket)
											<tr>
												<td><a class="btn btn-sm btn-primary" href="{{url('user/profile/'.User::find($ticket->author)->username)}}">{{User::find($ticket->author)->username}}</a></td>
												<td>{{$ticket->title}}</td>
												<td>{{$ticket->created_at}}</td>
												<td>
													<a class="btn btn-sm btn-primary" href="{{url('support/ticket/'.$ticket->id)}}">View</a>
													<a class="btn btn-sm btn-danger">Close</a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="tab-pane fade" id="v-pills-closed" role="tabpanel" aria-labelledby="v-pills-closed-tab">
								<table id="closed" class="table table-dark table-striped table-condensed">
									<thead>
									<tr>
										<th>User</th>
										<th>Title</th>
										<th>ClosedAT</th>
										<th>Action</th>
									</tr>
									</thead>
									<tbody>
									@foreach($tickets_closed as $ticket)
										<tr>
											<td><a class="btn btn-sm btn-primary" href="{{url('user/profile/'.User::find($ticket->author)->username)}}">{{User::find($ticket->author)->username}}</a></td>
											<td>{{$ticket->title}}</td>
											<td>{{$ticket->updated_at}}</td>
											<td>
												<a class="btn btn-sm btn-primary" href="{{url('support/ticket/'.$ticket->id)}}">View</a>
												<a class="btn btn-sm btn-danger">Re-Open</a>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
	<script type="text/javascript" src="{{url('vendor/datatables/datatables.min.js')}}"></script>
	<script defer>
      $(document).ready( function () {
          $('#open').DataTable({
		          "info": false,
              "lengthChange": false,
		          "search": {
                  "caseInsensitive": true,
                  "smart": true
		          },
          });
          $('#closed').DataTable({
              "info": false,
              "lengthChange": false,
              "search": {
                  "caseInsensitive": true,
                  "smart": true
              },
          });
      } );
	</script>
@endsection