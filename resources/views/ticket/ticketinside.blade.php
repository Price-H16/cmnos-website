<?php
	use App\User;
?>
@extends('layouts.app')
@section('title', '- Ticket - '.$ticket->title)
@section('css')
	<link rel="stylesheet" href="{{url('vendor/mybb/ui/trumbowyg.min.css')}}"/>
	<style>
		img {
			width: 35vw;
		}
		img[width] {
			max-width: 40vw;
			width: 40vw;
		}
	</style>
@endsection
@section('content')
	<div class="container bg-dark rounded mb-3">
		<div class="row mb-3">
			<div class="col-12 text-center text-white">
				<h1>{{$ticket->title}}</h1>
				@if($ticket->status === "CLOSED")
					<h2>This ticket was closed at {{$ticket->updated_at}}</h2>
				@endif
			</div>
		</div>
		<div class="row mb-3 px-5">
			@foreach($replays as $reply)
				<div class="{{ (User::find($reply->author)->access > 2 ? "col-10 ticket-admin" : "col-10 offset-2 ticket-reply") }}">
					<div class="row d-block px-3 pt-2">
						<div class="float-left">
							<a class="btn btn-primary btn-sm" href="{{url('user/profile/'.User::find($reply->author)->username)}}">
								{!! (User::find($reply->author)->access > 2 ? '<i class="fad fa-user-shield fa-fw fa-2x"></i>' : "") !!} {{ (strlen(User::find($reply->author)->displayname) > 2 ? User::find($reply->author)->displayname : User::find($reply->author)->username) }}
							</a>
						</div>
						<div class="float-right">{{$reply->created_at}}</div>
						<div class="clearfix"></div>
						<hr/>
					</div>
					<div class="row text-wrap px-3 d-block">
						{!! base64_decode($reply->messages) !!}
					</div>
				</div>
			@endforeach
		</div>
	</div>
	@if($ticket->status === "OPEN")
		<div class="container bg-dark mb-5 rounded">
			<div class="row mb-3 px-3 text-white">
				<form class="w-100" action="{{url('support/ticket/addreply')}}" method="POST">
					<input type="hidden" name="ticket_id" value="{{$ticket->id}}">
					{{csrf_field()}}
					<div class="form-group">
						<label for="reply_area">Reply to this ticket</label>
						<textarea class="form-control" id="reply_area" rows="3" name="reply_msg"></textarea>
					</div>
					<div class="form-group px-3 justify-content-center d-flex">
						<div class="btn-group" role="group" aria-label="Basic example">
							<button class="btn btn-danger" type="submit" name="action" value="close"><i class="far fa-lock fa-fw fa-2x"></i> Close Ticket</button>
							<button class="btn btn-success" type="submit" name="action" value="add"><i class="far fa-paper-plane fa-fw fa-2x"></i> Post Reply</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	@else
		<div class="container bg-dark mb-5 text-center rounded p-3">
			<h2 class="text-danger">This ticket is closed</h2>
			<h4 class="text-danger">If problem persist please open new ticket</h4>
		</div>
	@endif
@endsection
@section('js')
	<script src="{{url('vendor/mybb/trumbowyg.min.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
      $(document).ready(function () {
          $('#reply_area').trumbowyg({
              btns: [
                  ['undo', 'redo'],
                  ['formatting'],
		              ['insertImage'],
                  ['strong', 'em', 'del'],
                  ['link'],
                  ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                  ['removeformat'],
              ],
              imageWidthModalEdit: true,
              autogrow: true,
          });
      });
	</script>
@endsection