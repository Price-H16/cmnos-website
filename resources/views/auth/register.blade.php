@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card bg-dark text-white">
					<div class="card-header">{{ __('Register') }}</div>
					<div class="card-body">
						<form method="POST" action="{{ route('register') }}">
							@csrf
							<div class="form-group row">
								<label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
								<div class="col-md-6">
									<input id="username" type="text" class="form-control @error('name') is-invalid @enderror"
									       name="username" value="{{ old('name') }}" required autocomplete="username" autofocus>
									@error('name')
									<span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
									</span>
									@enderror
								</div>
							</div>
							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
								<div class="col-md-6">
									<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
									       value="{{ old('email') }}" required autocomplete="email">
									@error('email')
									<span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
									@enderror
								</div>
							</div>
							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
								<div class="col-md-6">
									<input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
									       name="password" required autocomplete="new-password">
									@error('password')
									<span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
									@enderror
								</div>
							</div>
							<div class="form-group row">
								<label for="password-confirm"
								       class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
								<div class="col-md-6">
									<input id="password-confirm" type="password" class="form-control" name="password_confirmation"
									       required autocomplete="new-password">
								</div>
							</div>
							@if(env('REG_TOS', false))
								<div class="form-group form-check text-center">
									<input type="checkbox" class="form-check-input" id="reg_tos" required>
									<label class="form-check-label" for="reg_tos">I agree with the <a href="{{url('terms-of-service')}}" target="_blank">Terms and Conditions</a></label>
								</div>
							@endif
							@if(env('REG_GR', false))
								<div class="form-group form-check text-center">
									<input type="checkbox" class="form-check-input" id="reg_gr" required>
									<label class="form-check-label" for="reg_gr">I agree with the <a href="{{url('game-rules')}}" target="_blank">Game Community Rules</a></label>
								</div>
							@endif
							@if(env('G_ENABLE', false))
							<div class="form-group row google_re">
								<div id="g-recaptcha"></div>
							</div>
							@endif
							<div class="form-group row mb-0">
								<div class="col-md-6 offset-md-5">
									<button type="submit" class="btn btn-primary" disabled>
										{{ __('Register') }}
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@if(env('G_ENABLE', false))
	@section('js')
		<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en&onload=onloadCallback&render=explicit" async defer></script>
		<script type="text/javascript">
	      var onloadCallback = function() {
	          var captchaContainer = document.querySelector('#g-recaptcha');
	          grecaptcha.render(captchaContainer, {
	              'sitekey' : '{{env('G_PUBLIC')}}',
			          'theme': 'dark'
	          });
	          document.querySelector('button[type="submit"]').disabled = false;
	      };
		</script>
	@endsection
@endif