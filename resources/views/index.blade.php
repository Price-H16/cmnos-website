@extends('layouts.app')

@section('title', '- Home')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-8 col-xl-9">
				<article role="article" id="post_{{rand(1,999)}}" class="post bg-secondary rounded mb-3">
					<div class="row">
						<div class="col-5">
							<img class="img-fluid" src="{{url('img/test.jpg')}}" alt="post_title"/>
						</div>
						<div class="col-7 py-2 px-1 text-light">
							<h4 class="text-center">Some Title</h4>
							<p>Some a lot of text</p>
						</div>
					</div>
				</article>
				
				<article role="article" id="post_{{rand(1,999)}}" class="post bg-secondary rounded">
					<div class="row">
						<div class="col-5">
							<img class="img-fluid" src="{{url('img/test.jpg')}}" alt="post_title"/>
						</div>
						<div class="col-7 py-2 px-1 text-light">
							<h4 class="text-center">Some Title</h4>
							<p>Some a lot of text</p>
						</div>
					</div>
				</article>
			</div>
			<div class="col-12 col-sm-12 col-md-4 col-xl-3">
				<div class="row bg-dark rounded">
					<div class="col-12 text-light">
						<h4 class="text-center">Server Status</h4>
						@foreach($server as $data)
							<p class="text-center">
								{{$data["title"]}} is {!! ($data["status"] ? "<span class='text-success'>Online</span>" : "<span class='text-danger'>Offline</span>") !!}
							</p>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
@stop