<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/html">
	<head>
		@include('layouts.headmeta')
    <title>{{env('APP_NAME', 'Some Server')}} @yield('title')</title>
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}" />
		<link rel="stylesheet" href="{{url('css/all.min.css')}}" />
		<link rel="stylesheet" href="{{url('css/toastr.min.css')}}">
    <link rel="stylesheet" href="{{url('css/main.css')}}" />
    @yield('css')
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm mb-3">
				<div class="container">
					<a class="navbar-brand" href="{{ url('/') }}">
						{{ config('app.name', 'Laravel') }}
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="{{url("/")}}"><i class="fas fa-home fa-fw"></i>Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="{{url("download")}}"><i class="fas fa-download fa-fw"></i>Download</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="{{url("ranking")}}"><i class="far fa-transporter-2 fa-fw"></i>Ranking</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="{{url("event")}}"><i class="fas fa-gifts fa-fw"></i>Events</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="https://discord.gg/g9p2p76"><i class="fab fa-discord fa-fw"></i>Discord</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDDKB" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-question-circle fa-fw"></i>Help</a>
								<div class="dropdown-menu" aria-labelledby="navbarDDKB">
									<a class="dropdown-item" href=""><i class="fas fa-scroll-old fa-fw"></i>Community Game Rules</a>
									<a class="dropdown-item" href=""><i class="fad fa-scroll-old fa-fw"></i>Terms of Service</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="{{url("support")}}"><i class="fal fa-ticket-alt fa-fw"></i>Support</a>
									<a class="dropdown-item" href=""><i class="fas fa-info-circle fa-fw"></i>F.A.Q.</a>
								</div>
							</li>
						</ul>
						<ul class="navbar-nav ml-auto">
							@guest
								<li class="nav-item">
									<a class="nav-link" href="{{ route('login') }}"><i class="far fa-sign-in fa-fw"></i>{{ __('Login') }}</a>
								</li>
							@if (Route::has('register'))
								<li class="nav-item">
									<a class="nav-link" href="{{ route('register') }}"><i class="far fa-user-plus fa-fw"></i>{{ __('Register') }}</a>
								</li>
							@endif
							@else
								<li class="nav-item dropdown">
									<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
										<i class="far fa-user fa-fw"></i>{{ Auth::user()->username }} <span class="caret"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarADMIN">
										@if(Auth::user()->access > 5)
											<div class="dropleft">
												<a class="dropdown-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Owner
												</a>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action 1</a>
													<a class="dropdown-item" href="#">Action 2</a>
												</div>
											</div>
											<div class="dropdown-divider"></div>
										@endif
										@if(Auth::user()->access > 4)
											<div class="dropleft">
												<a class="dropdown-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													GameAdministrator
												</a>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action 1</a>
													<a class="dropdown-item" href="#">Action 2</a>
												</div>
											</div>
											<div class="dropdown-divider"></div>
										@endif
										@if(Auth::user()->access > 3)
												<div class="dropleft">
													<a class="dropdown-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														GameMaster
													</a>
													<div class="dropdown-menu">
														<a class="dropdown-item" href="#">Action 1</a>
														<a class="dropdown-item" href="#">Action 2</a>
													</div>
												</div>
												<div class="dropdown-divider"></div>
										@endif
										@if(Auth::user()->access > 2)
												<div class="dropleft">
													<a class="dropdown-item dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														GameSupport
													</a>
													<div class="dropdown-menu">
														<a class="dropdown-item" href="#">Action 1</a>
														<a class="dropdown-item" href="#">Action 2</a>
													</div>
												</div>
												<div class="dropdown-divider"></div>
										@endif
										<a class="dropdown-item" href="#"><i class="fal fa-key-skeleton fa-fw"></i>Forgot Lockcode</a>
										<a class="dropdown-item" href="#"><i class="far fa-edit fa-fw"></i>Edit Profile</a>
										<a class="dropdown-item" href="#" data-toggle="modal" data-target="#ChangePasswordModal" id="CPM"><i class="fas fa-key fa-fw"></i>Change password</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
											<i class="far fa-sign-out fa-fw"></i>{{ __('Logout') }}
										</a>
										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											@csrf
										</form>
									</div>
								</li>
							@endguest
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<main role="content">
			@yield('content')
		</main>
		<footer class="mt-3">
			<nav class="navbar fixed-bottom navbar-dark bg-dark">
				<div class="container-fluid row">
					<div class="col-7 offset-5 text-white">
						&copy; {{date("Y")}} - {{env('APP_NAME', 'Some Server')}}
					</div>
				</div>
			</nav>
		</footer>
		@includeWhen(Auth::check(),'layouts.authmodal')
		<script src="{{url('js/jquery.min.js')}}" type="text/javascript"></script>
		<script src="{{url('js/bootstrap.bundle.min.js')}}" type="text/javascript"></script>
		<script src="{{url('js/toastr.min.js')}}" type="text/javascript"></script>
		<script src="{{url('js/main.js')}}" type="text/javascript"></script>
		@yield('js')
	</body>
</html>
