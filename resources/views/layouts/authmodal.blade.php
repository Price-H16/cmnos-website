<!-- Modal Zone -->
<!-- Change password -->
<div class="modal fade" id="ChangePasswordModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ChangePasswordModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered ">
		<div class="modal-content bg-dark text-white">
			<div class="modal-header">
				<h5 class="modal-title" id="ChangePasswordModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="form-group row">
						<label for="oldPassword" class="col-sm-6 col-form-label">Current Password</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" id="oldPassword">
						</div>
					</div>
					<div class="form-group row">
						<label for="newpassword" class="col-sm-6 col-form-label">New Password</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" id="newpassword">
						</div>
					</div>
					<div class="form-group row">
						<label for="confirmnewpassword" class="col-sm-6 col-form-label">Confrim Password</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" id="confirmnewpassword">
						</div>
					</div>
					{{csrf_field()}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Discharge</button>
				<button type="button" class="btn btn-success" id="pw_change" data-url="{{url('user/changepassword')}}">Change</button>
			</div>
		</div>
	</div>
</div>
