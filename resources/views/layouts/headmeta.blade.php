<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="theme-color" content="#1c1c1c">
<meta name="msapplication-navbutton-color" content="#1c1c1c">
<meta name="apple-mobile-web-app-status-bar-style" content="#1c1c1c">
<meta name="application-name" content="{{env('APP_NAME', 'Some Server')}}">
<meta name="apple-mobile-web-app-title" content="{{env('APP_NAME', 'Some Server')}}">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">

<link rel="image_src" href="{{url('img/favicon/256x256.png')}}">
{{--<link rel="icon" href="{{url('img/favicon/16x16.png')}}" sizes="16x16" type="image/png">--}}
{{--<link rel="icon" href="{{url('img/favicon/32x32.png')}}" sizes="32x32" type="image/png">--}}
{{--<link rel="icon" href="{{url('img/favicon/48x48.png')}}" sizes="48x48" type="image/png">--}}
{{--<link rel="icon" href="{{url('img/favicon/64x64.png')}}" sizes="64x64" type="image/png">--}}
{{--<link rel="icon" href="{{url('img/favicon/128x128.png')}}" sizes="128x128" type="image/png">--}}
<link rel="icon" href="{{url('img/favicon/256x256.png')}}" sizes="256x256" type="image/png">
{{--<link rel="apple-touch-icon" href="{{url('img/favicon/128x128.png')}}" sizes="128x128" type="image/png">--}}
{{--<link rel="apple-touch-icon" href="{{url('img/favicon/192x192.png')}}" sizes="192x192" type="image/png">--}}
<link rel="apple-touch-icon" href="{{url('img/favicon/256x256.png')}}" sizes="256x256" type="image/png">
{{--<link rel="apple-touch-icon" href="{{url('img/favicon/512x512.png')}}" sizes="512x512" type="image/png">--}}
{{--<link rel="apple-touch-startup-image" href="{{url('img/favicon/128x128.png')}}" type="image/png">--}}
{{--<link rel="apple-touch-icon-precomposed" href="{{url('img/favicon/192x192.png')}}" sizes="192x192" type="image/png">--}}

<meta property="og:type" content="website">
<meta property="og:locale" content="en_GB" />
<meta property="og:locale:alternate" content="fr_FR" />
<meta property="og:locale:alternate" content="es_ES" />
<meta property="og:locale:alternate" content="ro_RO" />
<meta property="og:locale:alternate" content="de_DE" />
<meta property="og:locale:alternate" content="pl_PL" />
<meta property="og:locale:alternate" content="tk_TK" />

<meta property="og:site_name" content="{{env('APP_NAME', 'Some Server')}}">
<meta property="og:title" content="{{env('APP_NAME', 'Some Server')}}">
<meta property="og:url" content="{{url('/')}}">
<meta property="og:image" content="{{url('img/favicon/256x256.png')}}">
<meta property="og:description" content="CMNos is an non-profit immersive anime online mass role playing game with your friends. More than 30 expert classes and loyal pets give exciting moments in PvP, PvE and raids.">

<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@cmnosofficial">
<meta name="twitter:creator" content="@cmnosofficial">
<meta name="twitter:image:src" content="{{url('img/favicon/256x256.png')}}">

<meta name="Description" content="CMNos is an non-profit immersive anime online mass role playing game with your friends. More than 30 expert classes and loyal pets give exciting moments in PvP, PvE and raids.">
<meta name="keywords" content="NosTale, private server, community, emulator, nostale emulator, nostale emu, nosemu">

<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "@id": "{{url('/')}}",
    "url": "{{url('/')}}",
    "name": "{{env('APP_NAME', 'Some Server')}}",
    "alternateName": "NosTale {{env('APP_NAME', 'Some Server')}}",
    "image": "{{url('img/favicon/256x256.png')}}",
    "logo": "{{url('img/favicon/256x256.png')}}",
    "isFamilyFriendly": "True",
    "keywords": "NosTale, private server, community, emulator, nostale emulator, nostale emu, nosemu",
    "thumbnailUrl": "{{url('img/favicon/256x256.png')}}",
    "headline": "{{env('APP_NAME', 'Some Server')}}",
    "creator": "{{env('APP_NAME', 'Some Server')}} Team",
    "isAccessibleForFree": "True"
  }
</script>

{{--<link rel="manifest" href="manifest.json">--}}