$(document).ready(function () {
    $("form").on('submit', function (e) {
       e.preventDefault();
       $("button[type=submit]").prop('disabled', true);
        $.ajax({
            url: $('form').attr('action'),
            data: {
                'username': $('#username').val(),
                'password': $('#password').val(),
                '_token': $('input[name="_token"]').val()
            },
            type: "POST",
            success: function (data) {
                var result = JSON.parse(atob(data));
                toastr[result.type](result.msg, result.title);
                if(result.type === "success")
                    window.location = '/';
                $("button[type=submit]").prop('disabled', false);
            },
            error: function (data) {
                toastr.error('Unknow error had happens. Please contact game support.');
            }
        });
    });
});